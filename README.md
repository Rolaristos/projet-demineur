
# Projet démineur

### Projet fait à 4 :

*Membres du projet:* *Roland, Ethan, Arthur, Maverick*

*GROUPE 13B*

*Chef de projet : Roland*

### Répartition pour le projet :

*Semaine 1 :*
Tout d'abord, dans le projet, nous avons commencé par faire le diagramme de classes en groupe. Ensuite, le professeur nous a donné le diagramme corrigé et nous avons pu commencer le code.

*Semaine 2 :*
Pour ce qui est du code, nous avons réparti les tâches. Chacun devait s'occuper d'une classe. Ensuite, deux membres du groupe se sont occupés des Docstrings et des commentaires des classes finies.

*Semaine 3 :*
Après ça, nous avons rajouté des classes pour pouvoir faire le javaFx et nous avons fait la correction des bugs.

Tout ce travail a été organisé par notre chef de projet, Roland.

### Commande pour générer la javadoc :

`javadoc --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls -d ./doc ./src/*.java`

### Commande pour compiler et exécuter avec javaFX :

*Pour compiler :*

`javac --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls *.java`

*Pour exécuter :*

`java --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls Executable`

### Bilan :

Le projet démineur était notre réel premier projet à faire. Ce projet nous a permis de revoir toutes les notions travaillées durant ce demi-semestre et d'apprendre de nouvelles compétences comme le javaFX qui était nouveau pour nous. Avec, par exemple, le  *ControleurBouton* . Il nous a aussi permis de travailler en équipe, même si GitLab n'a pas forcément toujours été utilisé pour la communication du groupe qui s'est faite non seulement par GitLab, mais aussi par l'utilisation de mails ou de clés USB. Donc, nous avons trouvé ce projet intéressant et ludique. Créer un jeu vidéo en Java nous a permis de travailler tous ensemble en nous amusant.
