import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Demineur extends Plateau{
    private boolean gameOver = false;
    private int score = 0;
    public Demineur(int nbLignes, int nbColonnes, int pourcentage){
        super(nbLignes, nbColonnes, pourcentage);
    }
    public int getScore(){
        return this.score;
    }

    public void reveler(int x, int y){
        CaseIntelligente c = super.getCase(x,y);
        List<List<Integer>> listIndices = new ArrayList<>();
        
        // Cette partie ne peut être réalisée qu'une seule fois par jeu. Elle permet d'éviter de tomber sur une bombe au premier clique.
        if (super.plateauEstVide() && !c.estMarquee()){ 
            if (c.contientUneBombe()){
            Random random = new Random();
            int xR = random.nextInt(this.getNbLignes());
            int yR = random.nextInt(this.getNbColonnes());
            int cpt = 0;
            // Condition nécessaire afin de ne pas poser la bombe sur une case où il y a déjà une bombe, ou sur la même case.
            while (xR == x && yR == y || !super.getCase(xR, yR).contientUneBombe() && cpt < getNbLignes()*getNbColonnes()){
                cpt+=1;
                xR = random.nextInt(this.getNbLignes());
                yR = random.nextInt(this.getNbColonnes());
            }
            super.poseBombes(xR, yR);
            }
            c.reset();
            c.reveler();
            if (c.nombreBombesVoisines() == 0){ // Condition essentielle car si la première case ne possède pas de bombes adjacentes, elles (les cases voisines) doivent toutes être découvertes
                reveler(x, y);
            }
        }

        // Cette partie permet de faire l'innondation. Le principe est simple :
        // Parcourir toute les cases voisines, On les mets dans une liste pour 
        // pouvoir faire un appel récursif sur ces dernières.
        else if (c.nombreBombesVoisines() == 0 && !c.contientUneBombe() && !c.estMarquee()){
            c.reveler();
            for (int i = x-1; i <= x+1; i++) {
                for (int j = y-1; j <= y+1; j++) {
                    if (i >= 0 && i < super.getNbLignes() && j >= 0 && j < super.getNbColonnes()) {
                        CaseIntelligente voisin = super.getCase(i, j);
                        if (!voisin.estDecouverte() && !voisin.contientUneBombe()) {
                            List<Integer> indices = new ArrayList<>();
                            indices.add(i);
                            indices.add(j);
                            voisin.reveler();
                            this.mettreScoreAJour();
                            reveler(i, j);
                        }
                    }
                }
            }

            // Fais un appel récursif sur les cases voisines de c n'ayant aucune bombes adjacentes.
            for (List<Integer> lesIndices : listIndices){
                reveler(lesIndices.get(0), lesIndices.get(1));
            }
        }

        // Cette partie signifie que le joueur a cliqué sur une bombe et donc que c'est perdu. Alors, on révèle toute les bombes
        else if (c.contientUneBombe() && !c.estMarquee()){ 
            c.reveler();
            for (int indiceX = 0; indiceX < super.getNbLignes(); indiceX++) {
                for (int indiceY = 0; indiceY < super.getNbColonnes(); indiceY++){
                    Case caseAReveler = super.getCase(indiceX, indiceY);
                    if (caseAReveler.contientUneBombe()){
                        caseAReveler.reveler();
                    }
                }
            }
            return;
        }

        // Cette partie signifie que la case ne possède pas de bombe mais possède des bombes adjacentes.
        // Donc, seul cette case est révélée.
        else{
            if (!c.estDecouverte() && !c.estMarquee()){
                c.reveler();
            }
        }
        // On met à jour le score à la fin de l'exécution des précédentes instructions.
        this.mettreScoreAJour();
    }
    
    public void marquer(int x, int y) {
        super.getCase(x,y).marquer();
    }

    public void mettreScoreAJour(){
        int cpt = 0;
        for (int x = 0; x < super.getNbLignes(); x++) {
            for (int y = 0; y < super.getNbColonnes(); y++){
                if (super.getCase(x, y).estDecouverte()){
                    cpt += 1;
                }
            }
        }
        this.score = cpt;
    }
    
    public boolean estGagnee() {
        for (int x = 0; x < super.getNbLignes(); x++) {
            for (int y = 0; y < super.getNbColonnes(); y++) {
                if (!super.getCase(x, y).estDecouverte() && !super.getCase(x,y).contientUneBombe()){
                    return false;
                }
            }
        }
        return true;
    }

    public boolean estPerdue() {
        for (int i = 0; i < super.getNbLignes(); i++) {
            for (int j = 0; j < super.getNbColonnes(); j++) {
                Case c = super.getCase(i, j);
                if (c.contientUneBombe() && c.estDecouverte()) {
                    this.gameOver = true;
                    return true;
                }
            }
        }
        return false;
    }

    public void reset(){
        super.reset();
        this.gameOver = false;
        this.score = 0;
    }

    public void affiche(){
        System.out.println("JEU DU DEMINEUR");
        // affichage de la bordure supérieure
        System.out.print("  ");
        for (int j=0; j<this.getNbColonnes(); j++){
            System.out.print("  "+j+" ");
        }
        System.out.print(" \n");
        
        // affichage des numéros de ligne + cases
        System.out.print("  ┌");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┬");
        }
        System.out.println("───┐");
        
        // affichage des numéros de ligne + cases
        for (int i = 0; i<this.getNbLignes(); i++){
            System.out.print(i+" ");
            for (int j=0; j<this.getNbColonnes(); j++){
                System.out.print("│ "+this.getCase(i, j).toString() + " ");
            }
            System.out.print("│\n");
            
            if (i!=this.getNbLignes()-1){
                // ligne milieu
                System.out.print("  ├");
                for (int j=0; j<this.getNbColonnes()-1; j++){
                        System.out.print("───┼");
                }
                System.out.println("───┤");
            }
        }

        // affichage de la bordure inférieure
        System.out.print("  └");
        for (int j=0; j<this.getNbColonnes()-1; j++){
                System.out.print("───┴");
        }
        System.out.println("───┘");
        
        // affichage des infos 
        System.out.println("Nombres de bombes à trouver : " + this.getNbTotalBombes());
        System.out.println("Nombres de cases marquées : " + this.getNbCaseMarquees());
        System.out.println("Score : " + this.getScore());
    }
    
    public void nouvellePartie(){
        this.reset();
        this.poseDesBombesAleatoirement();
        this.affiche();
        try (Scanner scan = new Scanner(System.in).useDelimiter("\n")) {
            while (!this.estPerdue() || !this.estGagnee()){
                System.out.println("Entrer une instruction de la forme R 3 2 ou M 3 2\npour Révéler/Marquer la case à la ligne 3 et à la colonne 2");
                String [] s = scan.nextLine().split(" ");
                String action = s[0];
                int x = Integer.valueOf(s[1]);
                int y = Integer.valueOf(s[2]);
                if (action.equals("M") || action.equals("m"))
                    this.marquer(x, y);
                else if (action.equals("R") || action.equals("r"))
                    this.reveler(x, y);
                this.affiche();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (this.gameOver){
            System.out.println("Oh !!! Vous avez perdu !");
        }
        else{
            System.out.println("Bravo !! Vous avez gagné !");
        }
    }
}