import java.util.ArrayList;
import java.util.List;
/**regarde les voisins des cases */
public class CaseIntelligente extends Case{
    private List<Case> lesVoisines;
    public CaseIntelligente() {
        super();
        this.lesVoisines = new ArrayList<>();
    }
    /** ajoute des voisin dans la liste des voisins de la case
     @param Case une case
     */
    public void ajouteVoisine(Case uneCase){
        this.lesVoisines.add(uneCase);
        
    }
    /** donne le nombre de bombe autour de la case
     @return int nombre
     */
    public int nombreBombesVoisines(){
        int nombre = 0;
        for(Case cases : this.lesVoisines){
            if(cases.contientUneBombe()){
                nombre+=1;
            }
        }
        return nombre;
    }
    @Override
    public String toString() {
        if(estDecouverte()){

            if (contientUneBombe()){
                return "@";
            }
            else if (nombreBombesVoisines()==0){
                return "0";
            }
            else{
                return ""+nombreBombesVoisines();
            }
        }
        else if(estMarquee()){
            return "M🏴";
        }
        else{
            return " ";
        }
    }
}