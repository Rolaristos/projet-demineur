/**
 * La classe `Case` représente une case individuelle dans un plateau de jeu Minesweeper.
 * Chaque `Case` peut contenir ou non une bombe, être découverte ou couverte, et être marquée ou non marquée.
 */
public class Case{
    private boolean contientUneBombe;
    private boolean estDecouverte;
    private boolean estMarquee;

    /**
     * Initialise une nouvelle instance de la classe `Case`.
    * Au départ, la case ne contient pas de bombe, n'est pas découverte et n'est pas marquée.
    */
    public Case(){
        this.contientUneBombe = false;
        this.estDecouverte = false;
        this.estMarquee = false;
    }

    /**
     * Réinitialise la case en remettant les valeurs de la bombe, de la découverte et de la marque à false.
     */
    public void reset(){
        this.contientUneBombe = false;
        this.estDecouverte = false;
        this.estMarquee = false;
    }

    /**
     * Place une bombe dans la case.
     */
    public void poseBombe(){
        this.contientUneBombe = true;
    }

    /**
     * @return `true` si la case contient une bombe, `false` sinon.
     */
    public boolean contientUneBombe(){
        return this.contientUneBombe;
    }


    /**
     * @return `true` si la case est découverte, `false` sinon.
     */
    public boolean estDecouverte(){
        return this.estDecouverte;
    }

    /**
     * @return `true` si la case est marquée, `false` sinon.
     */
    public boolean estMarquee(){
        return this.estMarquee;
    }

    /**
     * Découvre la case et retourne `true` si elle ne contient pas de bombe, `false` sinon.
     * @return `true` si la case ne contient pas de bombe, `false` sinon.
     */
    public boolean reveler(){
        this.estDecouverte = true;
        return ! this.contientUneBombe;
    }

    /**
     * Marque la case si elle n'est pas marquée, ou enlève la marque si elle est déjà marquée.
     */
    public void marquer(){
        if(!this.estMarquee){
            this.estMarquee = true;
        }
        else{
            this.estMarquee = false;
        }
    }
}