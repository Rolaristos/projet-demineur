import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Plateau{
    private int nbLignes;
    private int nbColonnes;
    private int pourcentageDeBombes;
    private int nbBombes;
    private List<CaseIntelligente> lePlateau;

    public Plateau(int nbLignes, int nbColonnes, int pourcentage){
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.pourcentageDeBombes = pourcentage;
        this.nbBombes = 0;
        this.lePlateau = new ArrayList<CaseIntelligente>();
        this.creerLesCasesVides();
        this.rendLesCasesIntelligentes();
        this.poseDesBombesAleatoirement();
    }

    public boolean plateauEstVide(){
        for (int x = 0; x < this.nbLignes; x++) {
            for (int y = 0; y < this.nbColonnes; y++) {
                if (this.getCase(x, y).estDecouverte()){
                    return false;
                }
            }
        }
        return true;
    }

    private void creerLesCasesVides(){
        for (int i=0;i<this.nbLignes*this.nbColonnes;i++){
            CaseIntelligente uneCase = new CaseIntelligente();
            this.lePlateau.add(uneCase);
        }
    }

    private void rendLesCasesIntelligentes() {
        for (int i = 0; i < this.nbLignes; i++) {
            for (int j = 0; j < this.nbColonnes; j++) {
                CaseIntelligente currentCase = this.getCase(i, j);
                List<CaseIntelligente> voisines = new ArrayList<>();
    
                // Ajouter les cases voisines dans la liste
                if (i > 0) {
                    if (j > 0) {
                        voisines.add((CaseIntelligente) this.getCase(i - 1, j - 1));
                    }
                    voisines.add((CaseIntelligente) this.getCase(i - 1, j));
                    if (j < this.nbColonnes - 1) {
                        voisines.add((CaseIntelligente) this.getCase(i - 1, j + 1));
                    }
                }
                if (j > 0) {
                    voisines.add((CaseIntelligente) this.getCase(i, j - 1));
                }
                if (j < this.nbColonnes - 1) {
                    voisines.add((CaseIntelligente) this.getCase(i, j + 1));
                }
                if (i < this.nbLignes - 1) {
                    if (j > 0) {
                        voisines.add((CaseIntelligente) this.getCase(i + 1, j - 1));
                    }
                    voisines.add((CaseIntelligente) this.getCase(i + 1, j));
                    if (j < this.nbColonnes - 1) {
                        voisines.add((CaseIntelligente) this.getCase(i + 1, j + 1));
                    }
                }
    
                // Associer les cases voisines à la case courante
                for (CaseIntelligente voisine : voisines) {
                    voisine.ajouteVoisine(currentCase);
                }
            }
        }
    }
    
    

    public void poseDesBombesAleatoirement(){
        int nbTotalCases = this.nbLignes * this.nbColonnes;
        int nbBombes = nbTotalCases * this.pourcentageDeBombes / 100;
        this.nbBombes = nbBombes;
        Random random = new Random();
        int i = 0;
        while (i < nbBombes) {
            int randomIndex = random.nextInt(nbTotalCases);
            CaseIntelligente uneCase = this.lePlateau.get(randomIndex);
            if (!uneCase.contientUneBombe()){
                uneCase.poseBombe();
                i += 1;
            }
        }
    }

    public int getNbLignes(){
        return this.nbLignes;
    }

    public int getNbColonnes(){
        return this.nbColonnes;
    }

    public int getNbTotalBombes(){
        return this.nbBombes;
    }

    public CaseIntelligente getCase(int numLigne, int numColonne){
        return this.lePlateau.get(numLigne*this.nbColonnes+numColonne);
    }

    public int getNbCaseMarquees(){
        int nbCasesMarquees = 0;
        for (int i = 0; i < this.nbLignes; i++) {
            for (int j = 0; j < this.nbColonnes; j++) {
                CaseIntelligente currentCase = this.getCase(i, j);
                if (currentCase.estMarquee()){
                    nbCasesMarquees++;
                }
            }
        }
        return nbCasesMarquees;
    }
    public void poseBombes(int x, int y){
        CaseIntelligente laCase = this.getCase(x, y);
        laCase.poseBombe();
    }
    

    public void reset(){
        for (int x = 0; x < this.getNbLignes(); x++) {
            for (int y = 0; y < this.getNbColonnes(); y++) {
                this.getCase(x, y).reset();
            }
        }
        this.poseDesBombesAleatoirement();
    }
    
}